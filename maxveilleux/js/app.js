(function($) {

	FastClick.attach(document.body);
	jQuery.fn.reverse = [].reverse;

	manageMenu();
	menuScrollTo();
	manageProjectsList();
	manageScrollFx();

	// Remove skrollr for iPad and less
	if ( $(window).width() > 1024 ){
    	skrollr.init();
    }

	// detect Edge
	if (document.documentMode || /Edge/.test(navigator.userAgent)) {
	    $('body').addClass('is-ie');
	}

	// Add body class if IE10
    if (Function('/*@cc_on return document.documentMode===10@*/')()){
        document.getElementsByTagName('body')[0].className+=' is-ie';
    }
    
    // Add body class if IE11
     var ie11Styles = [
     'msTextCombineHorizontal'];
    
     var d = document;
     var b = d.body;
     var s = b.style;
     var ieVersion = null;
     var property;
    
     for (var i = 0; i < ie11Styles.length; i++) {
         property = ie11Styles[i];

         if (s[property] != undefined) {
             $('body').addClass('is-ie');
         }
     }

/*=============================
FUNCTIONS
============================*/

function manageMenu(){

	$('header.navbar-default .navbar-nav').append('<li><a href="https://www.linkedin.com/in/maxime-veilleux-a83a6087/" class="icon-linkedin" target="_blank"></a></li>');

	$('#nav-icon').click(function(e){
		e.preventDefault();

		if ( $('.navbar-collapse').is(':visible') ){
			closeMenu();
		} else {
			openMenu();
		}
	});
}

function closeMenu(){

	$('.navbar-collapse .navbar-nav > li').reverse().each(function(index) {
        $(this).delay(150*index).animate({
            top: 10,
            opacity: 0
        }, 500);
    });

    setTimeout(function(){ $('.navbar-collapse').removeClass('fullwidth'); $('body').toggleClass('nav-opened'); }, 900);
    setTimeout(function(){ $('.navbar-collapse').removeClass('fullheight'); }, 1400);
    setTimeout(function(){ $('.navbar-collapse').hide(); }, 1900);
}

function openMenu(){

	$('.navbar-collapse').show();

	setTimeout(function(){ $('.navbar-collapse').addClass('fullheight'); }, 100);
	setTimeout(function(){ $('.navbar-collapse').addClass('fullwidth'); $('body').toggleClass('nav-opened'); }, 600);
	setTimeout(function(){ 
		$('.navbar-collapse .navbar-nav > li').delay(100).each(function(index){
            $(this).delay(150*index).animate({
                top: 0,
                opacity: 1
            }, 500);
        });
	}, 1100);
}

function menuScrollTo(){

	$('.navbar-collapse li > a').click(function(e){

		if ( !$(this).parent().is(':nth-last-child(2)') && !$(this).parent().is(':last-child') ){

			if ( $('body').hasClass('frontpage') ){
				e.preventDefault();

				closeMenu();

				var id = $(this).attr('href');
				var finalID = id.substring(id.indexOf("#")+1);

				setTimeout(function(){
					$("html, body").animate({ scrollTop: $('#'+finalID).offset().top }, 1200, 'easeInOutExpo');
				}, 1050);
			}
		}
	});
}

function manageProjectsList(){

	// If we're on home page
	if ( $('body.frontpage').length > 0 ){

		var id = 1;
		// Projects / list add data
		$('#projects .single-project').each(function(){
			$(this).attr('data-id', id);
			$(this).find('.small-subtitle').attr('data-project', id).clone().appendTo('#projects-list');
			id++;
		});

		// Manage projects list hover
		$('#projects-list > div').hover(function(){
			$(this).siblings().css('opacity', '0.25');
			var idHover = $(this).data('project');
			$('#projects .single-project[data-id='+idHover+']').addClass('hover');
		}, function(){
			$(this).siblings().css('opacity', '1');
			var idHover = $(this).data('project');
			$('#projects .single-project[data-id='+idHover+']').removeClass('hover');
		});

		// Manage projects hover
		$('#projects .single-project').hover(function(){
			var idHover = $(this).data('id');
			$('#projects-list > div[data-project='+idHover+']').siblings().css('opacity', '0.25');
		}, function(){
			var idHover = $(this).data('id');
			$('#projects-list > div[data-project='+idHover+']').siblings().css('opacity', '1');
		});

		// Manage scroll fx, projects list position
		$(window).scroll(function(){
			var projectsOffset = $('#projects .projects-inner').offset().top - 50;
			var projectsHeight = $('#projects .projects-inner').height();
			var projectsListHeight = $('#projects-list').height();

			if ( $(document).scrollTop() >= projectsOffset && $(document).scrollTop() < (projectsOffset + projectsHeight - projectsListHeight) ){
				if ( !$('#projects-list').hasClass('fixed') ){
					$('#projects-list').removeClass('fixed-bottom').addClass('fixed');
				}
			}
			if ( $(document).scrollTop() < projectsOffset ){
				if ( $('#projects-list').hasClass('fixed') ){
					$('#projects-list').removeClass('fixed-bottom').removeClass('fixed');
				}
			}
			if ( $(document).scrollTop() >= (projectsOffset + projectsHeight - projectsListHeight) ){
				$('#projects-list').removeClass('fixed').addClass('fixed-bottom');
			}
		});
	}
}

function manageScrollFx(){

	window.sr = ScrollReveal({
		viewFactor     :  0.55,
		origin         : 'bottom',
		distance       : '30px',
		duration       : 1000,
        reset          : true,
		scale          : 0,
        easing         : 'ease'
	});

    // General
    sr.reveal('.sr');

    if ( $(window).width() > 991 ){
    	sr.reveal('.sr-100', { delay: 100 });
	    sr.reveal('.sr-200', { delay: 200 });
	    sr.reveal('.sr-300', { delay: 300 });
	    sr.reveal('.sr-400', { delay: 400 });
	    sr.reveal('.sr-500', { delay: 500 });
	    sr.reveal('.sr-600', { delay: 600 });
	    sr.reveal('.sr-800', { delay: 800 });
	    sr.reveal('.sr-900', { delay: 900 });
    } 

    sr.reveal('.sr-dist-40', { distance: '45px' });
    sr.reveal('.sr-view-80', { viewFactor: 0.8 });
    sr.reveal('.sr-view-50', { viewFactor: 0.5 });
    sr.reveal('.sr-view-20', { viewFactor: 0.2 });
    sr.reveal('.sr-view-05', { viewFactor: 0.05 });
    sr.reveal('.sr-origin-right', { origin: 'right' });
    sr.reveal('.sr-origin-left', { origin: 'left' });
    sr.reveal('.sr-rotate-y', { rotate: { x: 0, y: 60, z: 0 }, viewFactor: 0.4 });
    sr.reveal('.sr-no-reset', { reset: false });

    sr.reveal('#technologies .wrap-technologies > div', { 
    	delay: 10,
    	duration: 20,
    	distance: '0px',
    	viewFactor: 0.1,
    	afterReveal: function(domEl){
    		$(domEl).find('p').css('visibility', 'visible');
    		$(domEl).find('p').shuffleLetters();
    	},
    	afterReset: function(domEl){
    		$(domEl).find('p').css('visibility', 'hidden');
    	}
    }, 80);

    sr.reveal('#projects .wrap-projects > div', { delay: 200, reset: false }, 150);

    sr.reveal('.divider', {
    	duration: 10,
    	distance: '0px',
    	viewFactor: 0.05,
    	reset: true,
    	afterReveal: function(domEl){
    		domEl.className += " anim";
    	},
    	afterReset: function(domEl){
    		domEl.classList.remove("anim");
    	}
    });
}

})( jQuery );
